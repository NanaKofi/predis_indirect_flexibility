import datetime
import random


class DataManager:

    def select_period(self, start, date: list, data: list, horizon: int = 24):
        print("START DATE: {}, select_perioding next {} values".format(start, horizon))
        end = start + datetime.timedelta(hours=horizon)
        if end > date[-1]:
            end = end - datetime.timedelta(hours=1)
            if end > date[-1]:
                end = date[-1]
            print("START: ", start)
            print("END : ", end)
        for unit in date:
            if unit == start:
                select_periodion = []
                d = []
                debut = date.index(unit)
                fin = date.index(end)

                for i in range(debut, fin):
                    select_periodion.append(data[i])
                    d.append(date[i])

                break

        return end, select_periodion, d

    def correction(self,df, cols = ["Charger_1", "Charger_2"], upper_limit= 14 ):
        indx = df.index
        df.reset_index(inplace = True)
        for i in range(len(indx) - 1):
            for col in cols:
                hold = df[col][indx[i]]

                #             print (hold)

                if hold > upper_limit and hold <= 21:
                    df[col][indx[i]] = 0
                    for x in range(2, -1, -1):

                        if hold > 7:
                            val = random.uniform(6, 7)

                        else:
                            val = hold

                        hold -= val
                        df[col][indx[i - x]] += val
                #

                elif hold > 21:
                    df[col][indx[i]] = 0
                    for x in range(2, -1, -1):

                        if hold - upper_limit > 0:
                            val = random.uniform(10.6, 13.94)

                        else:
                            val = hold

                        hold -= val
                        df[col][indx[i - x]] += val

                #
                else:
                    pass

        return df

    def restructure(self, df):
        d = []
        for index, frame in df.groupby([(df.index.month), (df.index.day)]):
            d.append(frame)
        return  d