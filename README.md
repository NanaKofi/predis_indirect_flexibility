# Indirect flexibility for sizing energy systems sizing

Abstract: The purpose of this paper is to provide a method for assessing the impact of direct and indirectflexibilities on the self-consumption of an office building. The goal is to assess how both the humanactors and technical interventions can affect or mitigate deviations in the self-consumption level ofa building from its optimal. This paper considers the Predis-MHi platform (a living lab) as a casestudy and applies a Mixed Integer Linear Programming optimization to manage both the direct(EV charging) and indirect flexibilities (battery charging). Our results indicate that the potential fora building’s self-consumption improvement using indirect flexibilities does exist, however, sincehuman behavior is uncertain, which is difficult to account for, technical interventions such asbattery storage can be used to mitigate the impact of having significant indirect flexibilities on abuilding’s energy performance. The results from this study could potentially be modeled into anindicator, which would serve to influence occupant behavior towards a desired optimal.
Keywords: Behavioural response, Greenhouse gas emissions, Optimisation, Self-sufficiency, Energy communities, Energy sufficiency.

# Participate
- Install `utils` using `pip install -e .` in the root folder.
- Add your data file ensuring that it has a column for:
    - PV production
    - Demand (withouth the indirect flexibility)
    - grid carbon impact
    - Indirect flexibility
- Propose new models in `battery_sizing/optimization`.
`
