import pandas as pd
# from pyomo.opt import SolverFactory
from pyomo.environ import *


class Optimize:

    def __init__(self, pv, demand, indirect_flex):
        self.pv = pv
        self.demand = demand
        self.indirect_flex = indirect_flex
        # self.data = df

    def _get_pyomo_timeseries(self, result, demand, production, dates, co2_grid, model, keys=None):
        """Custom function to retrive optimization timeseries from `base.py`"""

        if keys == None:
            keys = ["grid_import", "grid_export", "battery_in", "battery_out", "battery_energy", "charger_1",
                    "charger_2", "charger_3", "charger_4"]

        for key in keys:
            df = pd.DataFrame(index=["_"], data=getattr(model, key).get_values())
            df = df.transpose()
            df.columns = [key]
            result[key] = df[key].tolist()

        result = pd.DataFrame(result)
        result["battery_power"] = - (result["battery_in"] + result["battery_out"])
        result["production"] = production
        result["co2_grid"] = co2_grid
        result["load"] = demand
        result["dates"] = dates

        return result

    def simulate(self, max_dis: float = -40, max_chg: float = 50, ch_1=0, ch_2=0, ch_3=0, ch_4=0, ch_eff: float = 0.95,
                 dch_eff: float = 0.95, max_soc: float = 50, sd=1e-6, min_soc: float = 10, prodd: list = None,
                 loadd: list = None, init_cap: float = 50, datess: list = None, timestep=60, co2_grid=None,
                 solver='gurobi', verbose=False, solver_path=None,  obj="sc"):


        # max_dis :  maximum discharge powwer value of the battery (should be negative)
        # max_chg : maximum charging power valu of the battery
        # ch_eff : Battery charghing efficiency (between 1 and 0)
        # dch_eff : Battery discharghing efficiency (between 1 and 0)
        # sd : self-discharge rate of the battery
        # init_cap: initial capacity of the battery
        # timestep : the timestep foor simulation in minutes
        # max_soc : Maximum allowed state of charge (0 <= max_soc <= capacity)
        # min_soc : Minimum allowed state of charge (0 <= min_soc <= capacity)
        # prodd : list containing the PV production values (PV production profile)
        # loadd : list containing the demand profile
        # datess : currently unused
        # ch_1 : Energy consumed by charger 1 during the evaluation period
        # ch_2 : Energy consumed by charger 2 during the evaluation period
        # ch_3 : Energy consumed by charger 3 during the evaluation period
        # ch_4 : Energy consumed by charger 4 during the evaluation period
        # co2_grid : co2 emmisions of the grid in gCO2_eq/kwh
        # verbose: print out optimizer outputs
        # solver: LP solver to use for the optimization "gurobi" is default however change to "glpk" especially if using binder
        # obj: objective of the simulation ["co2" , "sc"] default val ="sc"

        # returns a dataframe with all the calculated porameters

        if sum(prodd) > 0:
            l_soc = 0.1
        else:
            l_soc = 0.03
        m = ConcreteModel()
        timestep = timestep / 60

        m.ts = Set(initialize=list(range(0, len(prodd))), ordered=True)
        last = m.ts.last()

        m.grid_import = Var(m.ts, domain=NonNegativeReals)
        m.battery_in = Var(m.ts, domain=NegativeReals)
        m.battery_out = Var(m.ts, domain=NonNegativeReals)
        m.battery_energy = Var(m.ts, domain=NonNegativeReals, bounds=(min_soc, max_soc))
        m.battery_state = Var(m.ts, domain=Binary, bounds=(0, 1))
        m.grid_export = Var(m.ts, domain=NegativeReals)
        m.charger_1 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))
        m.charger_2 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))

        m.charger_3 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))
        m.charger_4 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))

        m.end_cap = Var(domain=NonNegativeReals, bounds=(min_soc, max_soc))

        #################################################### Param
        m.self_consumption = Var(domain=NonNegativeReals)
        m.total_co2 = Var(domain=NonNegativeReals)

        #################################################### Rules
        # --------------------------------------------------------
        # ---------------------Battery----------------------------
        # --------------------------------------------------------

        def r_ev_charging_time(m, t):
            if datess[t].hour <= 7 or datess[t].hour > 20:
                return (m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t] == 0)
            else:
                return Constraint.Skip

        def r_charger_1_total(m):
            return (sum(m.charger_1[t] for t in m.ts) == ch_1)

        def r_charger_2_total(m):
            return (sum(m.charger_2[t] for t in m.ts) == ch_2)

        def r_charger_3_total(m):
            return (sum(m.charger_3[t] for t in m.ts) == ch_3)

        def r_charger_4_total(m):
            return (sum(m.charger_4[t] for t in m.ts) == ch_4)

        def r_battery_max_powerin(m, t):
            return (m.battery_in[t] >= -max_chg * m.battery_state[t])

        def r_battery_max_powerout(m, t):
            return (m.battery_out[t] <= max_dis * (m.battery_state[t] - 1))

        def r_battery_energy(m, t):
            if t == 0:
                return m.battery_energy[t] == init_cap
            else:
                return (m.battery_energy[t - 1] * (1 - sd) - m.battery_energy[t] ==
                        m.battery_in[t - 1] * timestep * ch_eff + m.battery_out[t - 1] * timestep / dch_eff)

        def r_battery_min_energy(m, t):
            return (m.battery_energy[t] >= min_soc)

        def r_battery_max_energy(m, t):
            return (m.battery_energy[t] <= max_soc)

        def r_end_cap(m):
            return (m.battery_energy[last] * (1 - sd) - m.battery_in[last] * timestep * ch_eff - m.battery_out[last]
                    * timestep / dch_eff == m.end_cap)

        def r_battery_max_end_energy(m):
            return (m.end_cap <= max_soc)

        def r_battery_min_end_energy(m):
            return (m.end_cap >= min_soc * (1 + l_soc))

        # --------------------------------------------------------
        # ------------------Grid imports--------------------------
        # --------------------------------------------------------
        def r_energy_balance(m, t):
            return (prodd[t] - (loadd[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])
                    + m.grid_export[t] + m.grid_import[t] + m.battery_in[t] + m.battery_out[t] == 0)

        def r_grid_export(m, t):
            return (-m.grid_export[t] <= prodd[t])

        def r_grid_import(m, t):
            return (m.grid_import[t] <= loadd[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])

        def r_co2(m):
            if obj == "co2":
                return (sum(m.grid_import[t] * co2_grid[t] for t in m.ts) == m.total_co2)
            else:
                return Constraint.Skip

        def r_self_consumption(m):

            return (sum(m.grid_import[t] for t in m.ts) - sum(m.grid_export[t] for t in m.ts)
                    == m.self_consumption)

        # --------------------------------------------------------
        # ---------------------Add To Model-----------------------
        # --------------------------------------------------------
        # Battery
        m.r1 = Constraint(m.ts, rule=r_battery_max_powerin)
        m.r2 = Constraint(m.ts, rule=r_battery_max_powerout)
        m.r3 = Constraint(m.ts, rule=r_battery_energy)
        m.r4 = Constraint(m.ts, rule=r_battery_min_energy)
        m.r5 = Constraint(m.ts, rule=r_battery_max_energy)
        m.r6 = Constraint(rule=r_battery_min_end_energy)
        m.r7 = Constraint(rule=r_battery_max_end_energy)

        m.r8 = Constraint(m.ts, rule=r_energy_balance)
        m.r9 = Constraint(m.ts, rule=r_grid_export)
        m.r10 = Constraint(rule=r_self_consumption)
        m.r11 = Constraint(m.ts, rule=r_grid_import)
        m.r12 = Constraint(m.ts, rule=r_ev_charging_time)
        m.r13 = Constraint(rule=r_charger_1_total)
        m.r14 = Constraint(rule=r_charger_2_total)

        m.r15 = Constraint(rule=r_charger_3_total)
        m.r16 = Constraint(rule=r_charger_4_total)
        m.r17 = Constraint(rule=r_end_cap)
        m.r18 = Constraint(rule=r_co2)

        def objective_function(m):
            if obj == "co2":
                return m.total_co2
            elif obj == "sc":
                return m.self_consumption


        m.objective = Objective(rule=objective_function, sense=minimize)

        m.write("utils/new.lp")
        with SolverFactory(solver, executable=solver_path) as opt:
            if solver == "glpk":
                opt.options['tmlim'] = 12
            results = opt.solve(m, tee=False)
            if verbose:
                print(results)
        return m, self._get_pyomo_timeseries(result=dict(), demand=loadd, production=prodd,
                                             co2_grid=co2_grid, dates=datess, model=m, keys=None)
