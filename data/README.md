# Data description (energy)
- Power profile for GrEn-ER electrical (engineering school building in Grenoble) PV production, consumption and 4 electric vehicles chargers from 01/2020 - 12/2021, with a one hour time step.
- Unit: kW


## Data licence

### Name
Creative Commons Attribution 4.0 International (CC BY 4.0)

### URL
https://creativecommons.org/licenses/by/4.0/

### Instructions
You are free: *To Share, To Create, To Adapt*; As long as you: *Attribute*  

### Files
Predis_data_2020_2021.csv

### Copyright
© Laboratoire de génie électrique de Grenoble (G2Elab)

# Data description (carbon intensity)
- French Grid carbon intensity (sourced from eco2mix)
- unit: g CO_2 eqv / kWh


# Data licence

### Name
Creative Commons Attribution 4.0 International (CC BY 4.0)

### URL
https://creativecommons.org/licenses/by/4.0/

### Instructions
All items of information, data or documents placed at the disposal of users are either the property of RTE or are provided subject to restricted rights of use and duplication.
### Files
Predis_data_2020_2021.csv

### Copyright
© Réseau de Transport d'Électricité (RTE)